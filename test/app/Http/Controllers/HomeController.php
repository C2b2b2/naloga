<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function AddKomentar(Request $request){
        DB::table('komentarji')->insert(['id_upo' => $request->input('id_upo'), 'komentar' => $request->input('komentar')]);
        return view('home');
    }
}
