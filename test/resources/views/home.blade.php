<!DOCTYPE html>

<?php
    $loggedAs = Auth::user();
?>

<html style="width:100%; height:100%;">
<head>
    <meta charset="utf-8"/>
    <title>Komentarji</title>
</head>
<body style="width:100%; height:100%; margin:0px;">
    
    <div style="float:right; margin-right: 20px;">
        <?php 
            echo "Prijavlen z " . $loggedAs->name;
        ?>
        <a href="{{ url('/logout') }}">Odjava</a>
    </div>

    <div style='margin-bottom:20px; margin-left:10px;'>
        <form action="{{URL::to('/home')}}" method="POST">
            {{ csrf_field() }}
            <input type='text' style='width:30%;' name='komentar'/>
            <input type='hidden' name='id_upo' value="<?php echo $loggedAs->id ?>">
            <input type='submit' value='Komentriaj'/>
        </form>
    </div>

<div style="width:40%; float:left; margin-left:10px;">
<?php
    $komentarji = DB::table('komentarji')->get();

    foreach($komentarji as $kom){
        echo "<div style='margin-bottom:5px; width:100%; height:20px; display:block; border-bottom:1px solid black;'>";
        $user = DB::table("users")->where('id', $kom->id_upo)->first();
        echo "<div style='float:left; margin-right:50px;'> Ime: " . $user->name . "</div>";
        echo "<div style='float:left'>" . $kom->komentar . "</div>";
        echo "<div style='float:right; margin-left:50px;'>" . $kom->time . "</div>";
        echo "</div>";
        echo "<br>";
    }    
                                                 
?>
</div>
</body>
</html>
